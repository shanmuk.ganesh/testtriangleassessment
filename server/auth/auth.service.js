const fs = require("fs");
const jwt = require("jsonwebtoken");
const config = require("../config.json");
const mongoose = require("mongoose");
const UserModel = mongoose.model("users");

// Create a token from a payload.
function createToken(payload) {
  const jwtConfig = {
    expiresIn: config.jwt.expiresIn
  };
  return jwt.sign(payload, config.jwt.secretKey, jwtConfig)
}
 
async function authenticate(username, password) {
  var docs = await  UserModel.find({$and: [{userName: username}, {password: password}]});
  if (docs.length !== 0) {
    console.info(`authenticateSuccess( ${username} )`,);
    return createToken({username})
  } else {
    console.warn(`authenticateFault()`);
    return null;
  }
}

async function register(req) {
var user = await UserModel.find({userName: req.body.userName}, {_id:1});
  if (user.length === 0) {
   await UserModel.create(req.body);
   const username = req.body.userName;
   return createToken({username});
  } else {
    console.warn(`registerFault( User "${req.body.userName}" already exists. )`);
    return null;
  }
}

module.exports = {
  authenticate,
  register
};
