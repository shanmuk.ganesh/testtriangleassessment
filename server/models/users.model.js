const mongoose = require("mongoose");


var UserSchema = new mongoose.Schema({

    userName: {
        type: String,
        required: "Required"
    },
    password: {
        type: String,
        required: "Required"
    },
    confirmPassword: {
        type: String,
        required: "Required"
    },
    email: {
        type: String,
        required: "Required"
    },
    displayName: {
        type: String
    },
    firstName: {
        type: String,
        required: "Required"
    },
    lastName: {
        type: String,
        required: "Required"
    },
    NickName: {
        type: String
    },
    Website: {
        type: String
    },
    Bio: {
        type: String
    },
    Jabber: {
        type: String
    },
    aolIm: {
        type: String
    },
    yahooIm: {
        type: String
    }
});

mongoose.model("users", UserSchema)
