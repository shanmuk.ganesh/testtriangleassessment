import {
    Component,
    OnInit
} from "@angular/core";
import {
    select,
    Store
} from "@ngrx/store";
import { Observable } from "rxjs";
import * as fromState from "../core/state";
import * as AuthActions from "../core/state/auth/auth.action";

@Component({
    selector: "blog-beer-list-container",
    template: `
	    <blog-beer-list
            (logout)="logout($event)"
	    >
	    </blog-beer-list>
    `
})
export class BeerListContainer implements OnInit {

    public constructor(private store$: Store<any>) {}
    public ngOnInit() { }

    public logout(event: any) {
        this.store$.dispatch(new AuthActions.NavigateToLogin());
    }
}
