import {
    ChangeDetectionStrategy,
    Component,
    Input,
    Output,
    EventEmitter,
    OnInit
} from "@angular/core";
import {
    RouterModule,
    Router
} from "@angular/router";
import { AuthService } from "../core/service/auth.service";

@Component({
    selector: "blog-beer-list",
    templateUrl: "./beer-list.component.html",
    styleUrls: ["./beer-list.component.scss"],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class BeerListComponent implements OnInit {

    @Output()
    public logout: EventEmitter<void> = new EventEmitter<void>();
    currentUser: any;
    tiles = [
        { cols: 3, rows: 1, color: '#ececec' },
        { cols: 1, rows: 1, color: '#ececec' },
        {  cols: 3, rows: 2, color: '#ececec' },
        { cols: 1, rows: 1, color: '#ececec' },
        { cols: 1, rows: 1, color: '#ececec' },
    ];
    sideMenuOptions = [
        { icon: 'dashboard', text: 'Dashboard' },
        { icon: 'search', text: 'Search' },
        { icon: 'account_circle', text: 'Account' },
        { icon: 'work', text: 'Work' },
        { icon: 'date_range', text: 'Schedules' },
        { icon: 'insert_drive_file', text: 'Files' },
        { icon: 'chat', text: 'Comments' },
        { icon: 'build', text: 'Settings' },
    ]
    public constructor(private authService: AuthService){
        this.authService.currentUser.subscribe(x => this.currentUser = x);
    }
    public ngOnInit() {
    }
    public onLogout(event: any) {
        this.authService.logout();
        this.logout.emit();
    }
}
