import {
    ActionReducerMap,
    createFeatureSelector,
    createSelector
} from "@ngrx/store";
import * as fromAuth from "./auth/auth.reducer";

export interface AppState {
    auth: fromAuth.AuthState;
}

export const reducers: ActionReducerMap<AppState> = {
    auth: fromAuth.authReducer
};

// -------------------------------------------------------------------
// AUTH SELECTORS
// -------------------------------------------------------------------
export const selectAuthState = createFeatureSelector<fromAuth.AuthState>("auth");

export const getToken = createSelector(
    selectAuthState,
    fromAuth.getToken
);

export const getError = createSelector(
    selectAuthState,
    fromAuth.getError
);

export const getPending = createSelector(
    selectAuthState,
    fromAuth.getPending
);

