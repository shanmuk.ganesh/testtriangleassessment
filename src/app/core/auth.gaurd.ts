import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from "../core/service/auth.service";
import {
    select,
    Store
} from "@ngrx/store";

import * as AuthActions from "../core/state/auth/auth.action";

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    constructor(
        private store$: Store<any>,
        private router: Router,
        private authenticationService: AuthService
    ) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        const currentUser = this.authenticationService.currentUserValue;
        if (currentUser) {
            return true;
        } 
        this.store$.dispatch(new AuthActions.NavigateToLogin());
        return false;
    }
}