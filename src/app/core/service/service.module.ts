import { NgModule } from "@angular/core";
import { ApiEndpointService } from "./api-endpoint.service";

const PROVIDERS = [
    ApiEndpointService
];

@NgModule({
    providers: PROVIDERS
})
export class ServiceModule {
}
