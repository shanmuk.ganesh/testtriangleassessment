import {
    HttpClient,
    HttpErrorResponse
} from "@angular/common/http";
import { Injectable } from "@angular/core";
import {
    Observable,
    throwError,
    BehaviorSubject
} from "rxjs";
import {
    catchError,
    map
} from "rxjs/operators";
import {
    Auth,
    AuthResponse,
    LoginCredentials,
    RegisterCredentials
} from "../domain/auth.model";
import * as CryptoJS from 'crypto-js';
import { ApiEndpointService } from "./api-endpoint.service";

@Injectable({
    providedIn: "root"
})
export class AuthService {
    private currentUserSubject: BehaviorSubject<any>;
    public currentUser: Observable<any>;
    privateKey: string = "0123456789123456";
    encrypted: '';
    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue() {
        return this.currentUserSubject.value;
    }

    public login(loginCredentials: LoginCredentials): Observable<Auth> {
        const url = ApiEndpointService.getEndpoint(ApiEndpointService.ENDPOINT.LOGIN);
        const pass = this.encryptUsingAES256(loginCredentials.password);
        const params = {
            userName: loginCredentials.userName,
            password: pass
        };
        return this.http.post(url, params).pipe(
            map((response: AuthResponse): Auth => {
                const user = {
                    ...params,
                    token: response.accessToken
                }
                localStorage.setItem('currentUser', JSON.stringify(user));
                this.currentUserSubject.next(user);
                return user;
            }),
            catchError((fault: HttpErrorResponse) => {
                console.warn(`loginFault( ${fault.error.message} )`);
                return throwError(fault);
            })
        );
    }

    encryptUsingAES256(password) {
        let _key = CryptoJS.enc.Utf8.parse(this.privateKey);
        let _iv = CryptoJS.enc.Utf8.parse(this.privateKey);
        let encrypted = CryptoJS.AES.encrypt(
            JSON.stringify(password), _key, {
            keySize: 16,
            iv: _iv,
            mode: CryptoJS.mode.ECB,
            padding: CryptoJS.pad.Pkcs7
        });
        return encrypted.toString();
    }

    public logout() {
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }

    public register(credentails: RegisterCredentials): Observable<Auth> {
        const url = ApiEndpointService.getEndpoint(ApiEndpointService.ENDPOINT.REGISTER);
        const encryptedPassword = this.encryptUsingAES256(credentails.password);
        const encConfirmPass = this.encryptUsingAES256(credentails.confirmPassword);
        const params = {
            userName: credentails.userName,
            password: encConfirmPass,
            email: credentails.email,
            confirmPassword: encConfirmPass,
            firstName: credentails.firstName,
            lastName: credentails.lastName,
            displayName: credentails.displayName,
            nickName: credentails.nickName,
            website: credentails.website,
            bio: credentails.bio,
            jabber: credentails.jabber,
            aolIm: credentails.aolIm,
            yahooIm: credentails.yahooIm
        };
        return this.http.post(url, params).pipe(
            map((response: AuthResponse): Auth => {
                console.info(`registerSuccess( Received access token: ${response.accessToken} )`);
                return {
                    ...params,
                    token: response.accessToken
                };
            }),
            catchError((fault: HttpErrorResponse) => {
                console.warn(`registerFault( ${fault.error.message} )`);
                return throwError(fault);
            })
        );
    }
}
