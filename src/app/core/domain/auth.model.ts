export interface LoginCredentials {
    userName: string;
    password: string;
}

export interface RegisterCredentials extends LoginCredentials {
    email: string;
    confirmPassword: string;
    displayName: string;
    firstName: string;
    lastName: string;
    nickName: string;
    website: string;
    bio: string;
    jabber: string;
    aolIm: string;
    yahooIm: string;
}

export interface Auth extends LoginCredentials {
    token: string;
}

export interface AuthResponse {
    accessToken: string;
}
