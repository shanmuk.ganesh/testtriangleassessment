import {
    Component,
    OnInit
} from "@angular/core";
import {
    select,
    Store
} from "@ngrx/store";
import { Observable } from "rxjs";
import { LoginCredentials } from "../../core/domain/auth.model";
import * as fromState from "../../core/state/";
import * as AuthActions from "../../core/state/auth/auth.action";

@Component({
    selector: "blog-login-container",
    template: `
		<blog-login
				[error]="error$ | async"
				[pending]="pending$ | async"
				(login)="login($event)"
				(register)="register($event)"
		>
		</blog-login>
    `
})
export class LoginContainer implements OnInit {
    public error$: Observable<string>;
    public pending$: Observable<boolean>;
    public constructor(private store$: Store<any>) {
    }
    public ngOnInit() {
        this.error$ = this.store$.pipe(select(fromState.getError));
        this.pending$ = this.store$.pipe(select(fromState.getPending));
    }
    public login(event: LoginCredentials) {
        this.store$.dispatch(new AuthActions.Login(event));
    }
    public register(event: any) {
        this.store$.dispatch(new AuthActions.NavigateToRegister());
    }
}
