import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output
} from "@angular/core";
import {
    FormBuilder,
    FormGroup, Validators
} from "@angular/forms";
import { LoginCredentials } from "../../core/domain/auth.model";
import * as FormUtil from "../../util/form.util";
import * as ValidationUtil from "../../util/validation.util";

@Component({
    selector: "blog-login",
    templateUrl: "./login.component.html",
    styleUrls: [ "./login.component.scss" ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit {
    @Input()
    public error = "";
    @Input()
    public pending = false;
    @Output()
    public login: EventEmitter<LoginCredentials> = new EventEmitter<LoginCredentials>();
    @Output()
    public register: EventEmitter<void> = new EventEmitter<void>();
    public loginForm: FormGroup;
    constructor(private formBuilder: FormBuilder) {
    }
    public ngOnInit(): void {
        this.loginForm = new FormGroup(
            this.formBuilder.group({
                userName: ['',Validators.required],
                password:['',Validators.required]
            }).controls,
            {
                updateOn: "blur"
            }
        );
    }
    public getFormValue(): LoginCredentials {
        return {
            userName: FormUtil.getFormFieldValue(this.loginForm, "userName"),
            password: FormUtil.getFormFieldValue(this.loginForm, "password")
        };
    }
    public onLogin(event: any) {
        if(this.loginForm.invalid) {
            this.error = "Please fill the required fields"
        }else {
            const payload: LoginCredentials = this.getFormValue();
            this.login.emit(payload);
        }
    }
    public onRegister(event: any) {
        this.register.emit();
    }
}
