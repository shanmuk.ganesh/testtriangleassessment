import {
    ChangeDetectionStrategy,
    Component,
    EventEmitter,
    Input,
    OnInit,
    Output
} from "@angular/core";
import {
    FormBuilder,
    FormGroup,
    Validators
} from "@angular/forms";
import { RegisterCredentials } from "../../core/domain/auth.model";
import * as FormUtil from "../../util/form.util";
import * as ValidationUtil from "../../util/validation.util";
import { MustMatch } from '../../util/must-match.validator';

@Component({
    selector: "blog-register",
    templateUrl: "./register.component.html",
    styleUrls: [ "./register.component.scss" ],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class RegisterComponent implements OnInit {
    @Input()
    public error = "";
    @Input()
    public pending = false;
    @Output()
    public register: EventEmitter<RegisterCredentials> = new EventEmitter<RegisterCredentials>();
    @Output()
    public cancel: EventEmitter<void> = new EventEmitter<void>();
    public registerForm: FormGroup;
    constructor(private formBuilder: FormBuilder) {
    }
    public ngOnInit(): void {
        this.registerForm = new FormGroup(
            this.formBuilder.group({
                userName: ['',Validators.required],
                email: ['', [Validators.required, Validators.email]],
                password: ['',[Validators.required,
                        Validators.maxLength(ValidationUtil.VALIDATION_RULE.PASSWORD.MAX_LENGTH)]
                ],
                confirmPassword: ['', Validators.required],
                displayName: [""],
                firstName: ['', Validators.required],
                lastName: ['', Validators.required],
                nickName: [""],
                website:[""],
                bio: [""],
                jabber: [""],
                aolIm: [""], yahooIm:[""]
            }, {
                validator: MustMatch('password', 'confirmPassword')
            }).controls,
            {
                updateOn: "blur"
            }
        );
    }
    public getFormValue(): RegisterCredentials {
        return {
            userName: FormUtil.getFormFieldValue(this.registerForm, "userName"),
            email: FormUtil.getFormFieldValue(this.registerForm, "email"),
            password: FormUtil.getFormFieldValue(this.registerForm, "password"),
            confirmPassword: FormUtil.getFormFieldValue(this.registerForm, "confirmPassword"),
            displayName: FormUtil.getFormFieldValue(this.registerForm, "displayName"),
            firstName: FormUtil.getFormFieldValue(this.registerForm, "firstName"),
            lastName: FormUtil.getFormFieldValue(this.registerForm,  "lastName"),
            nickName: FormUtil.getFormFieldValue(this.registerForm, "nickName"),
            website: FormUtil.getFormFieldValue(this.registerForm, "website"),
            bio: FormUtil.getFormFieldValue(this.registerForm, "bio"),
            jabber: FormUtil.getFormFieldValue(this.registerForm, "jabber"),
            aolIm: FormUtil.getFormFieldValue(this.registerForm, "aolIm"),
            yahooIm: FormUtil.getFormFieldValue(this.registerForm, "yahooIm")
        };
    }
    public onRegister(event: any) {
        if(this.registerForm.invalid) {
            this.error = "Please fill the required fields"
        }else {
            const payload: RegisterCredentials = this.getFormValue();
            console.log('payload is ', JSON.stringify(payload))
            this.register.emit(payload);
        }
    }
    public onCancel(event: any) {
        this.cancel.emit();
    }
}
