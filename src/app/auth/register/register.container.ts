import {
    Component,
    OnInit
} from "@angular/core";
import {
    select,
    Store
} from "@ngrx/store";
import { Observable } from "rxjs";
import { RegisterCredentials } from "../../core/domain/auth.model";
import * as fromState from "../../core/state";
import * as AuthActions from "../../core/state/auth/auth.action";

@Component({
    selector: "blog-register-container",
    template: `
		<blog-register
				[error]="error$ | async"
				[pending]="pending$ | async"
				(register)="register($event)"
				(cancel)="cancel($event)"
		>
		</blog-register>
    `
})
export class RegisterContainer implements OnInit {
    public error$: Observable<string>;
    public pending$: Observable<boolean>;
    public constructor(private store$: Store<any>) {
    }
    public ngOnInit() {
        this.error$ = this.store$.pipe(select(fromState.getError));
        this.pending$ = this.store$.pipe(select(fromState.getPending));
    }
    public register(event: RegisterCredentials) {
        this.store$.dispatch(new AuthActions.Register(event));
    }
    public cancel(event: any) {
        this.store$.dispatch(new AuthActions.NavigateToLogin());
    }
}
