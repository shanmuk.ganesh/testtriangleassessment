import { APP_BASE_HREF } from "@angular/common";
import { NgModule } from "@angular/core";
import {
    RouterModule,
    Routes
} from "@angular/router";
import { appRoutePaths } from "./app.routes";
import { AuthGuard } from "./core/auth.gaurd";

const PROVIDERS = [
    {
        provide: APP_BASE_HREF,
        useValue: "/"
    }
];

const routes: Routes = [
    {
        path: appRoutePaths.login,
        loadChildren: "./auth/login/login.module#LoginModule"
    },
    {
        path: appRoutePaths.register,
        loadChildren: "./auth/register/register.module#RegisterModule"
    },

    {
        path: appRoutePaths.beer,
        loadChildren: "./beer/beer.module#BeerModule",
        canActivate: [AuthGuard]
    },
    {
        path: "**",
        pathMatch: "full",
        redirectTo: appRoutePaths.login
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes, { useHash: false, enableTracing: false })
    ],
    exports: [ RouterModule ],
    providers: PROVIDERS
})
export class AppRoutingModule {
}
